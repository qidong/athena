/*
  Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
*/

#include "../PixelDefectsEmulatorCondAlg.h"
#include "../PixelDefectsEmulatorAlg.h"
#include "../StripDefectsEmulatorCondAlg.h"
#include "../StripDefectsEmulatorAlg.h"
#include "../PixelEmulatedDefectsToDetectorElementStatusCondAlg.h"
#include "../StripEmulatedDefectsToDetectorElementStatusCondAlg.h"

DECLARE_COMPONENT( InDet::PixelDefectsEmulatorCondAlg )
DECLARE_COMPONENT( InDet::PixelDefectsEmulatorAlg )
DECLARE_COMPONENT( InDet::PixelEmulatedDefectsToDetectorElementStatusCondAlg )

DECLARE_COMPONENT( InDet::StripDefectsEmulatorCondAlg )
DECLARE_COMPONENT( InDet::StripDefectsEmulatorAlg )
DECLARE_COMPONENT( InDet::StripEmulatedDefectsToDetectorElementStatusCondAlg )
